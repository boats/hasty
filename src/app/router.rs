use std::collections::HashMap;
use std::error::Error;
use std::sync::Arc;

use route_recognizer::Router as _Router;

use App;
use http::{self, Response, Request, Body};
pub use route_recognizer::Params;

#[derive(Default)]
pub struct Router {
    inner: HashMap<http::Method, _Router<Box<Endpoint>>>,
}

impl Router {
    pub fn new() -> Router {
        Router::default()
    }

    pub fn add(&mut self, method: http::Method, path: &str, endpoint: Box<Endpoint>) {
        self.inner.entry(method).or_insert(_Router::new()).add(path, endpoint);
    }

    pub fn recognize(&self, method: &http::Method, path: &str) -> Option<Match> {
        self.inner.get(method).and_then(|router| {
            router.recognize(path).ok().map(|app| Match {
                endpoint: &**app.handler,
                params: app.params,
            })
        })
    }
}

impl App for Router {
    fn serve(&self, req: Request<Body>) -> Result<Response<Body>, Box<Error>> {
        if let Some(m) = self.recognize(req.method(), req.uri().path()) {
            m.endpoint.serve(req, m.params)
        } else {
            Response::builder().status(http::StatusCode::NOT_FOUND)
                                  .body(Default::default())
                                  .map_err(Into::into)
        }
    }
}

#[macro_export]
macro_rules! router {
    ($($method:ident $path:expr => $app:expr,)*) => {{
        let mut router = $crate::router::Router::default();
        $(router.add($crate::http::Method::$method, $path, Box::new($app));)*
        router
    }};
}

pub struct Match<'a> {
    pub endpoint: &'a Endpoint,
    pub params: Params,
}

pub trait Endpoint: Send + Sync + 'static {
    fn serve(&self, req: Request<Body>, params: Params) -> Result<Response<Body>, Box<Error>>;
}

impl<F> Endpoint for F where
    F: Fn(Request<Body>, Params) -> Result<Response<Body>, Box<Error>> + Send + Sync + 'static,
{
    fn serve(&self, req: Request<Body>, params: Params) -> Result<Response<Body>, Box<Error>> {
        self(req, params)
    }
}

impl<T> Endpoint for Arc<T> where
    T: Endpoint
{
    fn serve(&self, req: Request<Body>, params: Params) -> Result<Response<Body>, Box<Error>> {
        (&**self).serve(req, params)
    }
}

impl Endpoint for Box<Endpoint> {
    fn serve(&self, req: Request<Body>, params: Params) -> Result<Response<Body>, Box<Error>> {
        (&**self).serve(req, params)
    }
}
