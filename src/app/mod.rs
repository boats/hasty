pub mod router;

use std::sync::Arc;
use std::error::Error;

use http::{Request, Response, Body, StatusCode};

pub trait App: Send + Sync + 'static {
    fn serve(&self, req: Request<Body>) -> Result<Response<Body>, Box<Error>>;

    fn error(&self, _: Box<Error>) -> Response<Body> {
        Response::builder().status(StatusCode::INTERNAL_SERVER_ERROR)
                           .body(Default::default())
                           .unwrap()
                    
    }

    fn should_shutdown(&self) -> bool {
        false
    }
}


impl<F> App for F where
    F: Fn(Request<Body>) -> Result<Response<Body>, Box<Error>> + Send + Sync + 'static
{
    fn serve(&self, req: Request<Body>) -> Result<Response<Body>, Box<Error>> {
        self(req)
    }
}

impl<T> App for Arc<T> where
    T: App
{
    fn serve(&self, req: Request<Body>) -> Result<Response<Body>, Box<Error>> {
        (&**self).serve(req)
    }
}

impl App for Box<App> {
    fn serve(&self, req: Request<Body>) -> Result<Response<Body>, Box<Error>> {
        (&**self).serve(req)
    }
}

pub trait Middleware: App + Sized {
    fn wrap<T>(inner: T) -> Self where T: App;

    fn chain<M>(self) -> M where M: Middleware {
        M::wrap(self)
    }
}
