use std::net::{SocketAddr, ToSocketAddrs};
use std::sync::Arc;

use cpupool::{self, CpuPool};
use hyper;
use futures;
use num_cpus;

use App;

pub fn serve<T>(app: T) where
    T: App,
{
    let addr = ("localhost", 7878).to_socket_addrs().unwrap().next().unwrap();
    println!("Running hasty \u{1f4a8} on localhost: http://{}", addr);
    serve_inner(addr, Arc::new(app))
}

pub fn localhost<T>(socket: u16, app: T) where T: App, {
    let addr = ("localhost", socket).to_socket_addrs().unwrap().next().unwrap();
    println!("Running hasty \u{1f4a8} on localhost: http://{}", addr);
    serve_inner(addr, Arc::new(app))
}

pub fn serve_with_addr<A, T>(addr: A, app: T) where
    A: ToSocketAddrs,
    T: App,
{
    let addr = addr.to_socket_addrs().unwrap().next()
                   .expect("serve_with_addr should receive at least one socket address");

    println!("Running hasty \u{1f4a8} on http://{}", addr);
    serve_inner(addr, Arc::new(app))
}

// A separate function to avoid monomorphization into end user applications
fn serve_inner(addr: SocketAddr, app: Arc<App>) {
    use hyper::server::Http;

    let pool = CpuPool::new(num_cpus::get() * 2);
    let shutdown = AppShutdown(app.clone());
    let service = AppService { app, pool };
    Http::new().bind(&addr, move || Ok(service.clone())).unwrap()
               .run_until(shutdown).unwrap();
}

#[derive(Clone)]
struct AppService {
    app: Arc<App>,
    pool: CpuPool,
}

impl hyper::server::Service for AppService {
    type Request = hyper::server::Request;
    type Response = hyper::server::Response;
    type Error = hyper::error::Error;
    type Future = cpupool::CpuFuture<Self::Response, Self::Error>;
    fn call(&self, req: Self::Request) -> Self::Future {
        let app = self.app.clone();
        self.pool.spawn_fn(move ||  {
            let req = req.into();
            match app.serve(req) {
                Ok(resp)    => futures::future::ok(resp.into()),
                Err(err)    => futures::future::ok(app.error(err).into()),
            }
        })
    }
}

struct AppShutdown(Arc<App>);

impl futures::Future for AppShutdown {
    type Item = ();
    type Error = ();
    fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
        if self.0.should_shutdown() { Ok(futures::Async::Ready(())) }
        else { Ok(futures::Async::NotReady) }
    }
}
