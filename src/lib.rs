extern crate futures;
extern crate futures_cpupool as cpupool;
extern crate http as _http;
extern crate hyper;
extern crate num_cpus;
extern crate route_recognizer;

mod app;
mod serve;
mod macros;

pub use app::*;
pub use serve::*;

pub mod http {
    pub use _http::*;
    pub use hyper::Body;
}

pub type Request = http::Request<http::Body>;
pub type Response = http::Response<http::Body>;
