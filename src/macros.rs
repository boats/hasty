#[macro_export]
macro_rules! html {
    ($($arg:tt)*) => {{
        let body: String = format!($($arg)*);
        $crate::http::Response::builder()
            .header($crate::http::header::CONTENT_TYPE, "text/html; charset=utf-8")
            .header($crate::http::header::CONTENT_LENGTH, &body.len().to_string()[..])
            .body(body.into())
            .unwrap()
    }};
}

#[macro_export]
macro_rules! static_content {
    ($path:expr) => {{
        let body: &[u8] = include_bytes!($path);
        let mime = match $path.rsplitn(2, ".").next().unwrap() {
            "txt"   => "text/plain; charset=utf-8",
            "html"  => "text/html; charset=utf-8",
            "css"   => "text/css; charset=utf-8",
            "js"    => "text/javascript; charset=utf-8",
            "jpeg"  => "image/jpeg",
            "png"   => "image/png",
            "gif"   => "image/gif",
            "pdf"   => "application/pdf",
            _       => "application/octet-stream",
        };
        $crate::http::Response::builder()
            .header($crate::http::header::CONTENT_TYPE, mime)
            .header($crate::http::header::CONTENT_LENGTH, &body.len().to_string()[..])
            .body(body.into())
            .unwrap()
    }};
}
