#[macro_use] extern crate hasty;

use hasty::router::Params;

fn main() {
    hasty::serve(router! {
        GET "/"             => |_, _| {
            Ok(html!("<html><body><h1>Echo Server!</h1></body></html>"))
        },
        GET "/echo/:echo"   => |_, params: Params| {
            let path = params["echo"].to_string();
            Ok(html!("<html><body>Echo: <b>{}</b></body></html>", path))
        },
    })
}
