extern crate hasty;

use hasty::http::{Request, Response};

fn main() {
    hasty::serve(|request: Request<_>| {
        let path = request.uri().path().to_string();
        Ok(Response::builder().body(path.into()).unwrap())
    })
}
